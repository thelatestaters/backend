package com.shopapplication.backend.controllers;

import com.shopapplication.backend.model.User;
import com.shopapplication.backend.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {
    @Autowired
    private UserRepository userRepository;

    @PostMapping(value = "/addUser")
    public User addUser(@RequestBody User user){

        userRepository.save(user);
        return userRepository.findById(String.valueOf(user.getId())).get();
    }


}
