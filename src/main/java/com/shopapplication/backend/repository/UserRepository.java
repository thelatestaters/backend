package com.shopapplication.backend.repository;

import com.shopapplication.backend.model.Customer;
import com.shopapplication.backend.model.User;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface UserRepository extends MongoRepository<User, String> {

}
