package com.shopapplication.backend;

import com.shopapplication.backend.model.Customer;
import com.shopapplication.backend.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BackendApplication{
    @Autowired
    private CustomerRepository repository;
    public static void main(String[] args) {
        SpringApplication.run(BackendApplication.class, args);
    }


}
